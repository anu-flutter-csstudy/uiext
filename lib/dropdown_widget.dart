import 'package:flutter/material.dart';

class DropDownWidget extends StatefulWidget {
  const DropDownWidget({Key? key}) : super(key: key);

  @override
  _DropDownWidgetState createState() => _DropDownWidgetState();
}

class _DropDownWidgetState extends State<DropDownWidget> {
  String vaccine = '-';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDown'),
      ),
      body: Column(
        children: [
          Container(
            child: Row(
              children: [
                Text('Vaccine :'),
                Expanded(
                  child: Container(),
                ),
                DropdownButton(
                  items: [
                    DropdownMenuItem(
                      child: Text('-'),
                      value: '-',
                    ),
                    DropdownMenuItem(
                      child: Text('Pfizer'),
                      value: 'Pfizer',
                    ),
                    DropdownMenuItem(
                      child: Text('J&J'),
                      value: 'J&J',
                    ),
                    DropdownMenuItem(
                      child: Text('Sputnik-v'),
                      value: 'Sputnik-v',
                    ),
                    DropdownMenuItem(
                      child: Text('Az'),
                      value: 'Az',
                    ),
                    DropdownMenuItem(
                      child: Text('Novavax'),
                      value: 'Novavax',
                    ),
                    DropdownMenuItem(
                      child: Text('Sinopharm'),
                      value: 'Sinopharm',
                    ),
                    DropdownMenuItem(
                      child: Text('Sinovac'),
                      value: 'Sinovac',
                    ),
                  ],
                  value: vaccine,
                  onChanged: (String? newValue) {
                    setState(() {
                      vaccine = newValue!;
                    });
                  },
                )
              ],
            ),
          ),
          Center(
              child: Text(
            vaccine,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ))
        ],
      ),
    );
  }
}
